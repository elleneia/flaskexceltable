from flask import Flask, render_template
import pandas as pd

app = Flask(__name__)

pd.set_option('display.width', 1000)
pd.set_option('colheader_justify', 'center')


def generate_html(dataframe: pd.DataFrame):  # function definition here
    table_html = dataframe.to_html(table_id="table", bold_rows=True, render_links=True)
    html = f"""
        {table_html}
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js" integrity="sha256-u7e5khyithlIdTpu22PHhENmPcRdFiHRjhAuHcs05RI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script>
            $(document).ready( function () {{
                $('#table').DataTable({{
                    // paging: false,    
                    // scrollY: 400,
                }});
            }});
        </script>
        """
    # return the html
    return html


@app.route("/")
def index():
    with open("links.xlsx"):
        df = pd.read_excel('links.xlsx', dtype={
            'postdate': 'datetime64', 'created': 'datetime64'}, index_col=None, header=0)
        df = df.fillna("")
        # return df.to_html(bold_rows=True)
        # content = df.to_html(bold_rows=True, render_links=True)
        html = generate_html(df)
        return render_template('main.html', content=html)


@app.route("/about")
def about():
    return render_template('about.html')


if __name__ == "__main__":
    app.run(debug=True)
